Start from nothing we will make a repo on github.

setup a directory on your machine called github. 

cd into github/

we're going to write the command:

git clone https://github.com/bghryct/git-for-typeDesign.git

or in your case clone from the repo you've made.

now we'll cd into the that repo (the local version of our git for typedesign)

cd git-for-typeDesign/

show how to see hidden files in finder
shift command . 
Write a .gitignore
(We only want git to track those changes that are useful to us. we don't need git to track autosaved files, or our virtual environment, etc. )

what goes in our .gitignore

venv    //////ignore our virtual environment
*(Autosaved)* //////ignore the annoying autosave behavior of glyphs
.DS_Store ////ignore apple metadata crap

Commit the .gitignore 
by writing

git add .gitignore
git commit -m "added .gitignore for font build"


Write a requirements.txt
(our requirements.txt is a shortcut to the names of all the of the dependencies of our font build. We want our colleagues to be on the same page as us in terms of what packages we're all using to build. This way we don't end up out of sync with the files that we're all working on collaboratively. Alternatively we can use this as a way to make sure people building our fonts from source if they're opensource, have the toolsets necessary to build without bloating our repo with a venv and all of the dependencies.)

Setup a virtual environment

install requirements.txt





next we will drop our directory contents from "initial state" into our repo
now we can practice adding and commiting with with the contents of the initial state directory we've just added to the git repo. 

